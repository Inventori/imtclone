﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPanel : MonoBehaviour
{
    public Text cps;
    public Text maxLoad;
    public Text loadingSpeed;
    public Text movementSpeed;

    private FacilityBase fBase;

    public void GetPanel(GameObject gObject)
    {
        //Get stats for each part by getting their base class
        fBase = gObject.GetComponent<FacilityBase>();

        //Set stats to text fields
        cps.text = "Capita per minute: " + Calculate.RoundToTwoDecimals(fBase.cps);
        maxLoad.text = "Max load: " + Calculate.RoundToTwoDecimals(fBase.maxLoad);
        loadingSpeed.text = "Loading speed: " + Calculate.RoundToTwoDecimals(fBase.loadingSpeed);
        movementSpeed.text = "Movement speed: " + Calculate.RoundToTwoDecimals(fBase.movingSpeed);

        //Set top left corner of the panel to mouse position
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(60, -60));
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }
}
