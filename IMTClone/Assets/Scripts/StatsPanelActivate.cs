﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class StatsPanelActivate : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    private StatsPanel panel;
    public GameObject fBase;

    public void Start()
    {
        //Get panel from GameManager
        panel = GameManager.instance.panel.GetComponent<StatsPanel>();
    }

    //When button is clicked, get new stats
    public void OnPointerClick(PointerEventData eventData)
    {
        panel.GetPanel(fBase);
    }

    //When mouse is hovered over button, set panel active and update stats on it.
    public void OnPointerEnter(PointerEventData eventData)
    {
        panel.gameObject.SetActive(true);
        panel.GetPanel(fBase);
    }

    //When mouse is hovered off button, disable panel
    public void OnPointerExit(PointerEventData eventData)
    {
        panel.gameObject.SetActive(false);
    }

}
