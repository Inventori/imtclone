﻿using UnityEngine;
using System.Collections;

public class TriggerEvent : MonoBehaviour
{
    public void TriggerOnClick(string triggerName)
    {
        //Trigger event with given name
        EventManager.TriggerEvent(triggerName);
    }
}
