﻿using UnityEngine;
using UnityEngine.UI;


public class FacilityBase : MonoBehaviour
{
    public float maxLoad;
    public float movingSpeed;
    public float loadingSpeed;
    public int level = 1;
    public float cps;
    public Text levelLabel;


    public Text minedAmount;
    public Text upgradeCostLabel;

    private bool automated;

    protected float cost;
    protected float baseCost;

    private void OnEnable()
    {
        EventManager.StartListening("UpdateLabels", UpdateLabels);
        automated = false;
    }
    private void OnDisable()
    {
        EventManager.StopListening("UpdateLabels", UpdateLabels);
    }

    private void Update()
    {
        if (automated)
            PerformRound();
    }

    public virtual void UpdateLabels()
    {
    }

    public virtual void PerformRound()
    {
    }

    public virtual void LevelUp()
    {
    }

    public virtual void SetAutomatic()
    {
        automated = !automated;
    }

    protected void DoStatUpgrade(int statMultiplier = 1)
    {
        cost = (int)Calculate.UpgradeCost(baseCost, 1.07f, level);

        GameManager.instance.SubtractMoney(cost);
        level++;
        levelLabel.text = "Level " + level;

        movingSpeed = Calculate.UpgradeStat(movingSpeed * .97f);
        maxLoad = Calculate.UpgradeStat(maxLoad);
        loadingSpeed = Calculate.UpgradeStat(loadingSpeed * 1.005f);

    }
}
