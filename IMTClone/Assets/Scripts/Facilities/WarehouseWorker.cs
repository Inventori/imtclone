﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WarehouseWorker : MonoBehaviour
{
    public bool busy;
    public int progress;

    private Warehouse warehouse;

    public Text status;
    public Image progressBar;

    float timer = 0;
    float waitingTime = 0;


    void Awake()
    {        
        //Get Mine component so miner has access to it's stats
        warehouse = GetComponentInParent<Warehouse>();
        busy = false;
        status.text = "Idle";
        progressBar.fillAmount = 0;
    }

    public IEnumerator GatherFromElevator()
    {        
        //Set busy true, so there are no multiple coroutines running for same object
        busy = true;
        float amountToFill = 0;
        timer = 0;

        while (Action("Moving", 1, warehouse.movingSpeed))
            yield return 0;

        //Get the amount of coal ready in elevator stash
        float coalInElevator = GameManager.instance.elevatorTotal;

        //Check if there is less coal in elevator than worker can carry
        if (coalInElevator < warehouse.maxLoad)
        {
            coalInElevator = GameManager.instance.elevatorTotal;
            amountToFill = coalInElevator;
        }

        //If capacity is exceeded, use maxLoad
        else
        {
            amountToFill = warehouse.maxLoad;
        }

        //Subtract from elevator stash, so no other worker try to claim same money
        warehouse.SubtractFromElevator(Calculate.RoundToWhole(amountToFill));

        while (Action("Collecting", amountToFill, warehouse.loadingSpeed))
            yield return 0;

        while (Action("Moving back", 1, warehouse.movingSpeed))
            yield return 0;

        while (Action("Unloading", amountToFill, warehouse.loadingSpeed))
            yield return 0;
      
        //Add collected money to warehouse
        warehouse.AddToTotal(amountToFill);

        //Update all labels
        EventManager.TriggerEvent("UpdateLabels");


        //Worker is now ready for another round
        progressBar.fillAmount = 0;
        busy = false;
        status.text = "Idle";

        yield return 0;
    }



    bool Action(string actionText, float x, float y)
    {
        //Calculate time needed for action by diving amount of work (x)
        //with speed of the worker (y)
        waitingTime = x / y;

        //Wait for timer to reach waitingTime
        if (timer < waitingTime)
        {
            ActionProgress(actionText);
            //Add to timer each frame
            timer += Time.deltaTime;
            return true;
        }
        else
        {
            //When time is up, reset the timer and return false so loop continues
            timer = 0;
            return false;
        }
    }

    void ActionProgress(string message)
    {
        //Progress bar gives visual representation for task at hand
        progressBar.fillAmount = Calculate.Percentage(timer, waitingTime) * 0.01f;
        status.text = message;
    }
}