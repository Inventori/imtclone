﻿using System.Collections.Generic;
using UnityEngine;

public class Warehouse : FacilityBase
{
    public GameObject warehouseworkerObject;

	private List<WarehouseWorker> workers = new List<WarehouseWorker>();

    void Start()
	{        
		movingSpeed = 1;
		maxLoad = 10;
		loadingSpeed = 6;
		baseCost = 10;
        minedAmount.text = "Money: " + GameManager.instance.totalMoney;
        upgradeCostLabel.text = "Upgrade " + baseCost;
        //Create first worker!
        CreateWorker();
	}

	public void CreateWorker()
    {       
        //Create worker and set it to child of this object
        GameObject warehouseWorker = Instantiate(warehouseworkerObject, transform);
        //Add miner to list of workers!
        workers.Add(warehouseWorker.GetComponent<WarehouseWorker>());
	}
    
	public override void PerformRound()
	{
        //Go through list of workers...
        foreach (WarehouseWorker worker in workers)
		{
            //if they are not busy...
            if (!worker.busy)
                //send them to do mining
                StartCoroutine(worker.GatherFromElevator());
		}
	}

	public void AddToTotal(float amount)
	{
		GameManager.instance.totalMoney += amount;
	}

	public void SubtractFromElevator(float amount)
	{
		GameManager.instance.elevatorTotal -= amount;
	}
	
	public override void LevelUp()
    {       
        //Upgrade stats
        DoStatUpgrade();
        //When level 20, 40, 60 etc. is reached, create a new worker!
        if (level % 20 == 0)
        {
            CreateWorker();
        }
        //Calctulate new capita per second when upgrade is done
        cps = Calculate.CapitaPerSecond(maxLoad, loadingSpeed, movingSpeed);
    }

    //UpdateLabels is called with a event trigger "UpdateLabels"
    public override void UpdateLabels()
    {
        upgradeCostLabel.text = "Upgrade " + cost;
        minedAmount.text = "Money: " + GameManager.instance.totalMoney;
    }
}
