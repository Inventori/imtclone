﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MineWorker : MonoBehaviour
{

    public Text status;
    public Image progressBar;
    public bool busy;

    private Mine mine;
    private float timer = 0;
    private float waitingTime = 0;

    void Awake()
    {
        //Get Mine component so miner has access to it's stats
        mine = GetComponentInParent<Mine>();
        busy = false;
        status.text = "Idle";
        progressBar.fillAmount = 0;
    }

    public IEnumerator GoMine()
    {
        //Set busy true, so there are no multiple coroutines running for same object
        busy = true;
        waitingTime = 0;
        timer = 0;

        while (Action("Walking", 1, mine.movingSpeed))
            yield return 0;

        while (Action("Mining", mine.maxLoad, mine.loadingSpeed))
            yield return 0;

        while (Action("Walking back", 1, mine.movingSpeed))
            yield return 0;

        //Add mined amount to total mined
        mine.AddToTotal(mine.maxLoad);

        //Update all the labels
        EventManager.TriggerEvent("UpdateLabels");


        //Worker is now ready for another round
        progressBar.fillAmount = 0;
        busy = false;
        status.text = "Idle";

        yield return 0;
    }


    void ActionProgress(string message)
    {
        //Progress bar gives visual representation for task at hand
        progressBar.fillAmount = Calculate.Percentage(timer, waitingTime) * 0.01f;
        status.text = message;
    }


    bool Action(string actionText, float x, float y)
    {
        //Calculate time needed for action by diving amount of work (x)
        //with speed of the miner (y)
        waitingTime = x / y;

        //Wait for timer to reach waitingTime
        if (timer < waitingTime)
        {
            ActionProgress(actionText);
            //Add to timer each frame
            timer += Time.deltaTime;
            return true;
        }
        else
        {            
            //When time is up, reset the timer and return false
            timer = 0;
            return false;
        }
    }
}

