﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class Elevator : FacilityBase
{
    private bool busy;
    public Text elevatorStatus;
    public Image progressBar;
    public float currentCapacity;

    private float timer;
    private float waitingTime;
    private float amountToFill;

    void Start()
    {
        movingSpeed = .5f;
        maxLoad = 40;
        loadingSpeed = 20;
        minedAmount.text = "Coal ready: " + 0;
        baseCost = 10;
        upgradeCostLabel.text = "Upgrade " + baseCost;
    }

    public override void PerformRound()
    {
        //If elevator is not doing a round, start coroutine
        if (!busy)
            StartCoroutine(MoveElevator());
    }

    //Add amount to elevator's fetched amount
    public void AddToTotal(float amount)
    {
        GameManager.instance.elevatorTotal += amount;
    }

    public IEnumerator MoveElevator()
    {       
        //Set busy true, so there are no multiple coroutines running for same object
        busy = true;
        bool elevatorFull = false;

        //Get number of mines from game manager
        int numberOfMines = GameManager.instance.mines.Count;

        //Distance is added with each mine visited
        float distance = 0;

        waitingTime = 0;

        for (int i = 0; i < numberOfMines; i++)
        {
            distance += 1;
            timer = 0;

            amountToFill = 0;

            //Get the amount of coal in mine from GameManager
            float coalInMine = GameManager.instance.mines[i].minedTotal;

            while (Action("Decending to mine number " + (i + 1), 1, movingSpeed))
                yield return 0;

            //Check if elevator capacity handles collected coal
            //If elevator can hold all the coal in mine...
            if (currentCapacity + coalInMine < maxLoad)
            {
                //Filled amount equals all the coal in mine
                amountToFill = coalInMine;
            }
            //If capacity is exceeded...
            else
            {
                //Calculate how much coal can be loaded until elevator is full
                amountToFill = maxLoad - currentCapacity;

                //Since the elevator is full, set boolean elevatorFull true
                elevatorFull = true;
            }

            while (Action("Loading from mine number " + (i + 1), amountToFill, loadingSpeed))
                yield return 0;

            //Add coal to overall capacity of elevator
            currentCapacity += amountToFill;
            //Remove coal from mine
            GameManager.instance.mines[i].SubtractFromMine(Calculate.RoundToWhole(amountToFill));

            //If elevator capacity is filled, exit loop and return to top
            if (elevatorFull)
            {
                break;
            }

            yield return 0;
        }

        while (Action("Moving to top", distance, movingSpeed))
            yield return 0;

        while (Action("Unloading", amountToFill, loadingSpeed))
            yield return 0;

        //Add mined coal to elevator stash
        AddToTotal(Calculate.RoundToWhole(currentCapacity));

        //Update all labels
        EventManager.TriggerEvent("UpdateLabels");

        //Elevator is now ready for another round
        progressBar.fillAmount = 0;
        currentCapacity = 0;
        elevatorStatus.text = "Idle";
        busy = false;

        yield return 0;
    }

    bool Action(string actionText, float x, float y)
    {
        //Calculate time needed for action by diving amount of work (x)
        //with speed of the elevator (y)
        waitingTime = x / y;

        //Wait for timer to reach waitingTime
        if (timer < waitingTime)
        {
            ActionProgress(actionText);
            //Add to timer each frame
            timer += Time.deltaTime;
            return true;
        }
        else
        {
            //When time is up, reset the timer and return false
            timer = 0;
            return false;
        }
    }

    void ActionProgress(string message)
    {
        //Progress bar gives visual representation for task at hand
        progressBar.fillAmount = Calculate.Percentage(timer, waitingTime) * 0.01f;
        elevatorStatus.text = message;
    }

    public override void LevelUp()
    {
        DoStatUpgrade();
        //Calculate capita per second for this facility
        //Since elevator loads and unloads, give loading speed parameter twice
        cps = Calculate.CapitaPerSecond(maxLoad, loadingSpeed * 2, movingSpeed);
    }

    public override void UpdateLabels()
    {
        minedAmount.text = "Coal ready: " + GameManager.instance.elevatorTotal;
        upgradeCostLabel.text = "Upgrade " + cost;
    }
}