﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Mine : FacilityBase
{
    public GameObject mineworkerObject;
    public float minedTotal;

    public Text nameLabel;

    private List<MineWorker> miners = new List<MineWorker>();
    private int mineNumber;

    void Start()
    {
        //Add this mine to list of mines in GameManager
        GameManager.instance.mines.Add(this);

        //What is the number of this mine? 
        //Higher the mines number, bigger the upgrades and starting stats! (and costs...)
        mineNumber = GameManager.instance.mines.Count;
        movingSpeed = 1;
        maxLoad = 10 * mineNumber;
        loadingSpeed = 6 * mineNumber;
        level = 1;
        minedAmount.text = "Coal ready: " + minedTotal;
        baseCost = 10 * mineNumber;
        upgradeCostLabel.text = "Upgrade " + baseCost;

        nameLabel.text = "Mine #" + mineNumber;

        //When mine is created, also create a miner
        CreateMiner();
    }

    public void CreateMiner()
    {
        //Create miner and set it to child of this object
        GameObject miner = Instantiate(mineworkerObject, transform);
        //Add miner to list of miners
        miners.Add(miner.GetComponent<MineWorker>());
    }

    public override void PerformRound()
    {
        //Go through list of miners...
        foreach (MineWorker mineWorker in miners)
        {
            //if they are not busy...
            if (!mineWorker.busy)
                //send them to do mining
                StartCoroutine(mineWorker.GoMine());
        }
    }

    public void AddToTotal(float amount)
    {
        minedTotal += Calculate.RoundToWhole(amount);
    }

    public void SubtractFromMine(float amount)
    {
        minedTotal -= Calculate.RoundToWhole(amount);
    }

    public override void LevelUp()
    {
        if (cost < GameManager.instance.totalMoney)
        {
            //Upgrade stats
            DoStatUpgrade();
            //When level 10, 20, 30 etc. is reached, create a new miner!
            if (level % 10 == 0)
            {
                CreateMiner();
            }
            //Calctulate new capita per second when upgrade is done
            cps = Calculate.CapitaPerSecond(maxLoad, loadingSpeed, movingSpeed);
        }
    }

    //UpdateLabels is called with a event trigger "UpdateLabels"
    public override void UpdateLabels()
    {
        minedAmount.text = "Coal ready: " + minedTotal;
        upgradeCostLabel.text = "Upgrade " + cost;
    }
}