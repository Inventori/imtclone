﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MineHandler : MonoBehaviour
{
    public GameObject mineObject;
    public Text buttonText;
    private GameObject mine;
    private int numberOfMines;

    private float cost;

    private void Start()
    {
        numberOfMines = 1;
        cost = 2000;
        buttonText.text = "Add mine " + cost;
    }


    //Create mew mine
    public void CreateMine()
    {
        if (cost < GameManager.instance.totalMoney)
        {
            GameManager.instance.SubtractMoney(cost);
            int i = 0;
            mine = Instantiate(mineObject, transform);
            //Get sibling index
            i = mine.transform.GetSiblingIndex();
            //Set mine one step up in hierarchy, so add mine button stays lowest
            mine.transform.SetSiblingIndex(i - 1);

            //Add one to number of mines, so the cost scales appropriately
            numberOfMines++;
            cost = Calculate.UpgradeCost(cost, 1.15f, numberOfMines);
            buttonText.text = "Add mine " + cost;
        }
    }
}
