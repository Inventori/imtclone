﻿using System;

public static class Calculate
{
    //For calculating how many percents a is from b
    //returns integer for whole percents
    public static int Percentage(float a, float b)
    {
        int p = 0;
        p = (int)((a / b) * 100);
        return p;
    }

    //For calculating cost of the upgrades of mines, elevator and factory
    public static float UpgradeCost(float baseCost, float multiplier, float owned)
    {
        float price = 0;
        price = baseCost * (float)Math.Pow(multiplier, owned);
        price = RoundToWhole(price);
        return price;
    }

    //For calculating CapitaPerSecond value
    public static float CapitaPerSecond(float maxLoad, float loadingSpeed, float movementSpeed)
    {
        float cpm, loadPerSecond, movementPerSecond;

        loadPerSecond = maxLoad / loadingSpeed;
        movementPerSecond = 1 / movementSpeed;
        cpm = maxLoad / (loadPerSecond + movementPerSecond * 2);

        return cpm;
    }

    //For calculating upgrade for given stat
    public static float UpgradeStat(float stat)
    {
        float newStat = 0;
        newStat = 1.05f * stat;
        return newStat;
    }

    //For rounding numbers to whole
    public static float RoundToWhole(float number)
    {
        float rounded = 0;
        //Convert float to decimal...
        decimal d = Convert.ToDecimal(number);
        //...so it can be rounded using Math.Round (while being cast back to float)
        rounded = (float)Math.Round(d, 0);
        return rounded;
    }

    //For rounding numbers to two decimals
    public static float RoundToTwoDecimals(float number)
    {
        float rounded = 0;
        //Convert float to decimal...
        decimal d = Convert.ToDecimal(number);
        //...so it can be rounded using Math.Round (while being cast back to float)
        rounded = (float)Math.Round(d, 2);
        return rounded;
    }
}