﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public float elevatorTotal;
    public float totalMoney;
    public List<Mine> mines;
    public GameObject panel;

    void Awake()
    {
        if (instance != null)
            Destroy(instance);
        else
            instance = this;

        DontDestroyOnLoad(this);
    }

    public void SubtractMoney(float amountToSubtract)
    {
        if (totalMoney > amountToSubtract)
        {
            totalMoney -= amountToSubtract;
            EventManager.TriggerEvent("UpdateLabels");
        }
    }
}